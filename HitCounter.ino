// Common CATHODE display
// Velky displej s blikanim
const int pinA = 2;
const int pinB = 3;
const int pinC = 4;
const int pinD = 5;
const int pinE = 6;
const int pinF = 7;
const int pinG = 8;
const int P1 = 9;
const int P2 = 10;
const int P3 = 11;
const int P4 = 12;

const int pinSensor = 13;

int currentCount = 0;
unsigned int cycleLength = 60 * 1000;
int previousRead = LOW;
boolean firstHit = true;
unsigned long lastMillis = 0;
unsigned long lastHitMillis = 0;
boolean correctCycle = true;
boolean countingCycle = true;
boolean counting = false;
boolean off = false;

void one();
void two();
void three();
void four();
void five();
void six();
void seven();
void eight();
void nine();
void zero();
void dash(int pos);
void display(int number, int pos);
void display(int number);

void setup()
{
  pinMode(pinA, OUTPUT);
  pinMode(pinB, OUTPUT);
  pinMode(pinC, OUTPUT);
  pinMode(pinD, OUTPUT);
  pinMode(pinE, OUTPUT);
  pinMode(pinF, OUTPUT);
  pinMode(pinG, OUTPUT);
  pinMode(P1, OUTPUT);
  pinMode(P2, OUTPUT);
  pinMode(P3, OUTPUT);
  pinMode(P4, OUTPUT);

  pinMode(pinSensor, INPUT);
  Serial.begin(9600);
}

void loop()
{
  if (countingCycle)
  {
    if (digitalRead(pinSensor) == LOW && previousRead == HIGH)
    {
      previousRead = LOW;
      //Serial.println("sensor LOW");
    }
    if (digitalRead(pinSensor) == HIGH && previousRead == LOW)
    {
      if (!firstHit)
      {
        currentCount++;
        counting = true;
        lastHitMillis = millis();
        //Serial.println(currentCount);
      }
      firstHit = !firstHit;
      previousRead = HIGH;
      //Serial.println("sensor HIGH");
      if (currentCount == 1)
        lastMillis = millis();
    }

    display(currentCount);

    if (counting)
    {
      if ((millis() - lastHitMillis >= 7500))
      {
        correctCycle = false;
        counting = false;
        countingCycle = false;
      }
      if (millis() - lastMillis >= cycleLength)
      {
        counting = false;
        countingCycle = false;
      }
    }
  }
  else
  {
    if (correctCycle)
    {
      if (millis() - lastMillis <= 1000)
      {
        //Serial.print("ON ");
        display(currentCount);
        off = false;
      }
      else if (millis() - lastMillis > 1000 &&
               millis() - lastMillis <= 1400)
      {
        if (!off)
        {
          turnOff();
          off = true;
        }
      }
      else
      {
        lastMillis = millis();
      }
    }
    else
      dash(4);
  }
}

void display(int number)
{
  int ones = 0, tens = 0, hunds = 0, tous;
  ones = number % 10;
  tens = ((number % 100) - ones) / 10;
  hunds = ((number % 1000) - tens) / 100;
  tous = ((number % 10000) - hunds) / 1000;
  display(ones, 4);
  if (number > 9)
    display(tens, 3);
  if (number > 99)
    display(hunds, 2);
  if (number > 999)
    display(tous, 1);
}

void display(int number, int pos)
{
  switch (number)
  {
  case 0:
    zero(pos);
    break;
  case 1:
    one(pos);
    break;
  case 2:
    two(pos);
    break;
  case 3:
    three(pos);
    break;
  case 4:
    four(pos);
    break;
  case 5:
    five(pos);
    break;
  case 6:
    six(pos);
    break;
  case 7:
    seven(pos);
    break;
  case 8:
    eight(pos);
    break;
  case 9:
    nine(pos);
    break;
  }
}

void turnOff()
{
  digitalWrite(P4, HIGH);
  digitalWrite(P3, HIGH);
  digitalWrite(P2, HIGH);
  digitalWrite(P1, HIGH);
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, LOW);
  digitalWrite(pinC, LOW);
  digitalWrite(pinD, LOW);
  digitalWrite(pinE, LOW);
  digitalWrite(pinF, LOW);
  digitalWrite(pinG, LOW);
}

void one(int pos)
{
  digitalWrite(P1, HIGH);
  digitalWrite(P2, HIGH);
  digitalWrite(P3, HIGH);
  digitalWrite(P4, HIGH);

  switch (pos)
  {
  case 1:
    digitalWrite(P1, LOW);
    break;
  case 2:
    digitalWrite(P2, LOW);
    break;
  case 3:
    digitalWrite(P3, LOW);
    break;
  case 4:
    digitalWrite(P4, LOW);
    break;
  }
  // 2
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, HIGH);
  digitalWrite(pinC, HIGH);
  digitalWrite(pinD, LOW);
  digitalWrite(pinE, LOW);
  digitalWrite(pinF, LOW);
  digitalWrite(pinG, LOW);
  delay(2);
}

void two(int pos)
{
  digitalWrite(P1, HIGH);
  digitalWrite(P2, HIGH);
  digitalWrite(P3, HIGH);
  digitalWrite(P4, HIGH);

  switch (pos)
  {
  case 1:
    digitalWrite(P1, LOW);
    break;
  case 2:
    digitalWrite(P2, LOW);
    break;
  case 3:
    digitalWrite(P3, LOW);
    break;
  case 4:
    digitalWrite(P4, LOW);
    break;
  }
  // 2
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, HIGH);
  digitalWrite(pinC, LOW);
  digitalWrite(pinD, HIGH);
  digitalWrite(pinE, HIGH);
  digitalWrite(pinF, LOW);
  digitalWrite(pinG, HIGH);
  delay(2);
}

void three(int pos)
{
  digitalWrite(P1, HIGH);
  digitalWrite(P2, HIGH);
  digitalWrite(P3, HIGH);
  digitalWrite(P4, HIGH);

  switch (pos)
  {
  case 1:
    digitalWrite(P1, LOW);
    break;
  case 2:
    digitalWrite(P2, LOW);
    break;
  case 3:
    digitalWrite(P3, LOW);
    break;
  case 4:
    digitalWrite(P4, LOW);
    break;
  }
  // 2
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, HIGH);
  digitalWrite(pinC, HIGH);
  digitalWrite(pinD, HIGH);
  digitalWrite(pinE, LOW);
  digitalWrite(pinF, LOW);
  digitalWrite(pinG, HIGH);
  delay(2);
}

void four(int pos)
{
  digitalWrite(P1, HIGH);
  digitalWrite(P2, HIGH);
  digitalWrite(P3, HIGH);
  digitalWrite(P4, HIGH);

  switch (pos)
  {
  case 1:
    digitalWrite(P1, LOW);
    break;
  case 2:
    digitalWrite(P2, LOW);
    break;
  case 3:
    digitalWrite(P3, LOW);
    break;
  case 4:
    digitalWrite(P4, LOW);
    break;
  }
  // 2
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, HIGH);
  digitalWrite(pinC, HIGH);
  digitalWrite(pinD, LOW);
  digitalWrite(pinE, LOW);
  digitalWrite(pinF, HIGH);
  digitalWrite(pinG, HIGH);
  delay(2);
}

void five(int pos)
{
  digitalWrite(P1, HIGH);
  digitalWrite(P2, HIGH);
  digitalWrite(P3, HIGH);
  digitalWrite(P4, HIGH);

  switch (pos)
  {
  case 1:
    digitalWrite(P1, LOW);
    break;
  case 2:
    digitalWrite(P2, LOW);
    break;
  case 3:
    digitalWrite(P3, LOW);
    break;
  case 4:
    digitalWrite(P4, LOW);
    break;
  }
  // 2
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, LOW);
  digitalWrite(pinC, HIGH);
  digitalWrite(pinD, HIGH);
  digitalWrite(pinE, LOW);
  digitalWrite(pinF, HIGH);
  digitalWrite(pinG, HIGH);
  delay(2);
}

void six(int pos)
{
  digitalWrite(P1, HIGH);
  digitalWrite(P2, HIGH);
  digitalWrite(P3, HIGH);
  digitalWrite(P4, HIGH);

  switch (pos)
  {
  case 1:
    digitalWrite(P1, LOW);
    break;
  case 2:
    digitalWrite(P2, LOW);
    break;
  case 3:
    digitalWrite(P3, LOW);
    break;
  case 4:
    digitalWrite(P4, LOW);
    break;
  }
  // 2
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, LOW);
  digitalWrite(pinC, HIGH);
  digitalWrite(pinD, HIGH);
  digitalWrite(pinE, HIGH);
  digitalWrite(pinF, HIGH);
  digitalWrite(pinG, HIGH);
  delay(2);
}

void seven(int pos)
{
  digitalWrite(P1, HIGH);
  digitalWrite(P2, HIGH);
  digitalWrite(P3, HIGH);
  digitalWrite(P4, HIGH);

  switch (pos)
  {
  case 1:
    digitalWrite(P1, LOW);
    break;
  case 2:
    digitalWrite(P2, LOW);
    break;
  case 3:
    digitalWrite(P3, LOW);
    break;
  case 4:
    digitalWrite(P4, LOW);
    break;
  }
  // 2
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, HIGH);
  digitalWrite(pinC, HIGH);
  digitalWrite(pinD, LOW);
  digitalWrite(pinE, LOW);
  digitalWrite(pinF, LOW);
  digitalWrite(pinG, LOW);
  delay(2);
}

void eight(int pos)
{
  digitalWrite(P1, HIGH);
  digitalWrite(P2, HIGH);
  digitalWrite(P3, HIGH);
  digitalWrite(P4, HIGH);

  switch (pos)
  {
  case 1:
    digitalWrite(P1, LOW);
    break;
  case 2:
    digitalWrite(P2, LOW);
    break;
  case 3:
    digitalWrite(P3, LOW);
    break;
  case 4:
    digitalWrite(P4, LOW);
    break;
  }
  // 2
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, HIGH);
  digitalWrite(pinC, HIGH);
  digitalWrite(pinD, HIGH);
  digitalWrite(pinE, HIGH);
  digitalWrite(pinF, HIGH);
  digitalWrite(pinG, HIGH);
  delay(2);
}

void nine(int pos)
{
  digitalWrite(P1, HIGH);
  digitalWrite(P2, HIGH);
  digitalWrite(P3, HIGH);
  digitalWrite(P4, HIGH);

  switch (pos)
  {
  case 1:
    digitalWrite(P1, LOW);
    break;
  case 2:
    digitalWrite(P2, LOW);
    break;
  case 3:
    digitalWrite(P3, LOW);
    break;
  case 4:
    digitalWrite(P4, LOW);
    break;
  }
  // 2
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, HIGH);
  digitalWrite(pinC, HIGH);
  digitalWrite(pinD, HIGH);
  digitalWrite(pinE, LOW);
  digitalWrite(pinF, HIGH);
  digitalWrite(pinG, HIGH);
  delay(2);
}

void zero(int pos)
{
  digitalWrite(P1, HIGH);
  digitalWrite(P2, HIGH);
  digitalWrite(P3, HIGH);
  digitalWrite(P4, HIGH);

  switch (pos)
  {
  case 1:
    digitalWrite(P1, LOW);
    break;
  case 2:
    digitalWrite(P2, LOW);
    break;
  case 3:
    digitalWrite(P3, LOW);
    break;
  case 4:
    digitalWrite(P4, LOW);
    break;
  }
  // 2
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, HIGH);
  digitalWrite(pinC, HIGH);
  digitalWrite(pinD, HIGH);
  digitalWrite(pinE, HIGH);
  digitalWrite(pinF, HIGH);
  digitalWrite(pinG, LOW);
  delay(2);
}

void dash(int pos)
{
  digitalWrite(P1, HIGH);
  digitalWrite(P2, HIGH);
  digitalWrite(P3, HIGH);
  digitalWrite(P4, HIGH);

  switch (pos)
  {
  case 1:
    digitalWrite(P1, LOW);
    break;
  case 2:
    digitalWrite(P2, LOW);
    break;
  case 3:
    digitalWrite(P3, LOW);
    break;
  case 4:
    digitalWrite(P4, LOW);
    break;
  }
  // 2
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, LOW);
  digitalWrite(pinC, LOW);
  digitalWrite(pinD, LOW);
  digitalWrite(pinE, LOW);
  digitalWrite(pinF, LOW);
  digitalWrite(pinG, HIGH);
  delay(2);
}
